#include <algorithm>

#include "gtest/gtest.h"
#include "LinkedList.hpp"
#include <vector>
using namespace std;


TEST(linkedlist_test, basic_add) {
	LinkedList<int>* list_ptr = new LinkedList<int>();
	LinkedList<int>& list = *list_ptr;

	list.add(1);
	list.add(2);
	list.add(3);
	list.add(4);
	list.add(5);
	list.add(6);

	vector<int> ans = {1, 2, 3, 4, 5, 6};
	vector<int>& result = list.toVector();

	EXPECT_TRUE(ans == result);

	delete &result;
	delete list_ptr;
}

TEST(linkedlist_test, basic_insert) {
	LinkedList<int>* list_ptr = new LinkedList<int>();
	LinkedList<int>& list = *list_ptr;

	list.add(1);
	list.add(2);
	list.add(5);
	list.add(6);
	list.add(2, 3);

	vector<int> ans = {1, 2, 3, 5, 6};
	vector<int>& result = list.toVector();

	EXPECT_TRUE(ans == result);

	delete &result;
	delete list_ptr;
}

TEST(linkedlist_test, medium_insert) {
	LinkedList<int>* list_ptr = new LinkedList<int>();
	LinkedList<int>& list = *list_ptr;

	list.add(1);
	list.add(2);
	list.add(5);
	list.add(6);
	list.add(2, 3);
	list.add(0, 12);
	list.add(list.size(), 21);

	vector<int> ans = {12, 1, 2, 3, 5, 6, 21};
	vector<int>& result = list.toVector();

	EXPECT_TRUE(ans == result);

	delete &result;
	delete list_ptr;
}


TEST(linkedlist_test, basic_remove) {
	LinkedList<int>* list_ptr = new LinkedList<int>();
	LinkedList<int>& list = *list_ptr;

	list.add(1);
	list.add(2);
	list.add(3);
	list.add(4);
	list.add(5);
	int removed = list.remove(3);

	EXPECT_EQ(removed, 4);

	vector<int> ans = {1, 2, 3, 5};
	vector<int>& result = list.toVector();

	EXPECT_TRUE(ans == result);

	delete &result;
	delete list_ptr;
}

TEST(linkedlist_test, medium_remove) {
	LinkedList<int>* list_ptr = new LinkedList<int>();
	LinkedList<int>& list = *list_ptr;

	list.add(1);
	list.add(2);
	list.add(3);
	list.add(4);
	list.add(5);
	int removed1 = list.remove(0);
	int removed2 = list.remove(3);

	EXPECT_EQ(removed1, 1);
	EXPECT_EQ(removed2, 5);

	vector<int> ans = {2, 3, 4};
	vector<int>& result = list.toVector();

	EXPECT_TRUE(ans == result);

	delete &result;
	delete list_ptr;
}

TEST(linkedlist_test, empty_and_reuse_front) {
	LinkedList<int>* list_ptr = new LinkedList<int>();
	LinkedList<int>& list = *list_ptr;

	list.add(1);
	list.add(2);
	list.add(3);
	list.add(4);
	list.add(5);
	list.remove(0);
	list.remove(0);
	list.remove(0);
	list.remove(0);
	list.remove(0);
	list.add(5);
	list.add(4);
	list.add(3);
	list.add(2);
	list.add(1);

	vector<int> ans = {5, 4, 3, 2, 1};
	vector<int>& result = list.toVector();

	EXPECT_TRUE(ans == result);

	delete &result;
	delete list_ptr;
}

TEST(linkedlist_test, empty_and_reuse_end) {
	LinkedList<int>* list_ptr = new LinkedList<int>();
	LinkedList<int>& list = *list_ptr;

	list.add(1);
	list.add(2);
	list.add(3);
	list.add(4);
	list.add(5);
	list.remove(4);
	list.remove(3);
	list.remove(2);
	list.remove(1);
	list.remove(0);
	list.add(5);
	list.add(4);
	list.add(3);
	list.add(2);
	list.add(1);

	vector<int> ans = {5, 4, 3, 2, 1};
	vector<int>& result = list.toVector();

	EXPECT_TRUE(ans == result);

	delete &result;
	delete list_ptr;
}

TEST(linkedlist_test, empty_and_reuse_mixed) {
	LinkedList<int>* list_ptr = new LinkedList<int>();
	LinkedList<int>& list = *list_ptr;

	list.add(1);
	list.add(2);
	list.add(3);
	list.add(4);
	list.add(5);
	list.remove(4);
	list.remove(0);
	list.remove(0);
	list.remove(1);
	list.remove(0);
	list.add(5);
	list.add(4);
	list.add(3);
	list.add(2);
	list.add(1);

	vector<int> ans = {5, 4, 3, 2, 1};
	vector<int>& result = list.toVector();

	EXPECT_TRUE(ans == result);

	delete &result;
	delete list_ptr;
}
