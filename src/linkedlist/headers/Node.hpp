
/*
  elevator-linked-list is a protoype linked list based on how elevators in
  tall buildings are built.

  Copyright (C) 2015  Jay Kamat

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A class to represent a data node in a linkedlist
 *
 * @author Jay Kamat
 * @version 1.0
 */

#pragma once

using namespace std;

template <class T>
class Node {

public:
	Node(T& data) {
		init(data, nullptr, nullptr);
	}

	~Node() {
		// Don't need to free anything
	}

	T& getData() {
		return data;
	}

	void setData(T& in) {
		data = in;
	}

public:
	Node(T& data, Node<T>* previous, Node<T>* next) {
		init(data, previous, next);
	}

	void setNext(Node<T>* next) {
		this->next = next;
	}

	void setPrevious(Node<T>* previous) {
		this->previous = previous;
	}

	void setLower(Node<T>* lower) {
		this->lower = lower;
	}

	Node<T>* getNext() {
		return this->next;
	}

	Node<T>* getPrevious() {
		return this->previous;
	}

	Node<T>* getLower() {
		return this->lower;
	}

private:
	T data;
	Node<T>* next;
	Node<T>* previous;
	Node<T>* lower;

	void init(T& data, Node<T>* previous, Node<T>* next) {
		this->data = data;
		this->next = next;
		this->previous = previous;
		this->lower = nullptr;
	}
};
