/*
  elevator-linked-list is a protoype linked list based on how elevators in
  tall buildings are built.

  Copyright (C) 2015  Jay Kamat

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*******************************************************************************
 * DIAGRAM OF STRUCTURE
 *
 * Threshold = 2
 *
 * 3 +---------------------->o
 *   |                       |
 * 2 +---------->o<--------->o
 *   |           |           |
 * 1 +---->o<--->o<--->o<--->o<--->o
 *   |     |     |     |     |     |
 * 0 o<>o<>o<>o<>o<>o<>o<>o<>o<>o<>o
 *
 * Overall, it's much like a skip list, but its way more organized so we can
 * get some guarenteed big O's.
 *
 * It can also be visualized as a binary tree, in worst case. (start from the
 * highest skylobby and work your way down, one movement per level).
 *
 * Unlike a binary tree or skip list, however, it's elements are indexed, and
 * can be reterived given an index in O(log n) time.
 *
 * **Tentative Time and Space Complexities**
 *
 * 't' is the threshold value
 *
 * |-----------+--------+-----------------|
 * | Operation | Type   | Time Complexity |
 * |-----------+--------+-----------------|
 * | Get       | Random | O(log_t^n)      |
 * | Insert    | Random | O(n)            |
 * | Remove    | Random | O(n)            |
 * | Append    | End    | O(1)            |
 * | Dequeue   | End    | O(1)            |
 * |-----------+--------+-----------------|
 *
 * Space Complexity: O(n log_t^n)
 *
 * Skylobby 1 needs some special work to allow for insertion/deletion, but all the ones
 * above that one can assume we only remove/add from the end, which allows for O(n) insertion/removal
 *
 *******************************************************************************/

/**
 * A class to represent a linkedlist that uses elevator mechanics to improve performance of location of elements
 * @author Jay Kamat
 */

#pragma once

#include <stdexcept>
#include <iostream>
#include "Node.hpp"
#include "LinkedList.hpp"
#include <cmath>
using namespace std;

template <class T>
class ElevatorLinkedList : public LinkedList<T> {

public:
	ElevatorLinkedList(int threshold) {
		if (threshold <= 1) {
			ostringstream oss;
			oss << "Threshold cannot be less than 1. Threshold was: " << threshold << endl;
			throw std::invalid_argument(oss.str());
		}

		this->threshold = threshold;
	}

	ElevatorLinkedList() {
		this->threshold = 50;
	}

	~ElevatorLinkedList() {
		if (upper != nullptr) {
			delete upper;
			upper = nullptr;
		}

		// Implicit destructor on superclass
	}

	// This is for debugging/testing only
	virtual ElevatorLinkedList<T>* getUpper() {
		return upper;
	}

	virtual string toString() override {
		ostringstream oss;
		if (upper != nullptr) {
			oss << upper->toString();
		}
		oss << this->LinkedList<T>::toString() << endl;

		return oss.str();
	}

	virtual T remove(int index) override {
		T toReturn = LinkedList<T>::remove(index);

		// Don't need to do any reshuffling if the upper node dosent exist
		if (upper != nullptr && upper->size() > 0) {
			Node<T>* toEdit = upper->getReference(getUpperIndex(index));

			if ((getUpperIndex(index) + 1) * this->threshold <= index) {
				// We should NOT change the placement of this node, only the next ones
				toEdit = toEdit->getNext();
			}

			while (toEdit != nullptr) {
				if (toEdit->getLower()  != nullptr) {
					if (toEdit->getLower() == nullptr || toEdit->getLower()->getNext() == nullptr) {
						// We want to remove this node. The only case where this can occur is when we are at the end.
						if (toEdit != upper->tail) {
							ostringstream oss;
							oss << "An unknown error occured! Please report this." << index << endl;
							throw std::invalid_argument(oss.str());
						}
						this->upper->remove(this->upper->size() - 1);

						// We shouldn't need to worry about what happens if upper becomes empty but lets do it just in case
						if (this->upper->size() == 0) {
							delete this->upper;
							this->upper = nullptr;
						}

						break;
					}

					toEdit->setLower(toEdit->getLower()->getNext());

					// This is useful for debugging higher levels, dosent impact time complexity
					toEdit->setData(toEdit->getLower()->getData());
				}
				// this will be null if we have not set the node yet (ie the last node).
				toEdit = toEdit->getNext();
			}
		}
		return toReturn;
	}

protected:
	int threshold;
	ElevatorLinkedList<T>* upper = nullptr;


	/**
	 * Internal backend for adding elements
	 */
	virtual Node<T>* internalAdd(int index, T& data) override {
		// This ::internalAdd will use our getReference to get the ref as quickly as possible (or head/tail)
		Node<T>* addedNode = LinkedList<T>::internalAdd(index, data);
		Node<T>* upperNode = nullptr;

		if (this->size() % threshold == 0) {
			// We just passed a threshold!
			this->setupUpper();
			// Add a node to the upper list to accommidate that we passed a threshold
			// TODO The addednode->getData() isn't used and is taking up space. This keeps things simple though.
			upperNode = upper->internalAdd(upper->size(), addedNode->getData());
			// cout << "Added a node to the upper list" << upper->toString() << endl; // Comment out for fun
		}

		// upperNode has already been inserted due to the superclass's internalAdd
		if (index == this->size() - 1) {
			// Handle edge case of appending to list (set new upper node to appended node)
			if (upperNode != nullptr) {
				upperNode->setLower(addedNode);
			}
		} else {
			// Generic O(n) insert
			// This part is simply fixing the upper structure because the list changed.

			// This part relies on having thresholds >= 2


			// The node to edit from
			if (upper != nullptr && upper->size() > 0) {
				Node<T>* toEdit = upper->getReference(getUpperIndex(index));

				if ((getUpperIndex(index) + 1) * this->threshold <= index) {
					// We should NOT change the placement of this node, only the next ones
					toEdit = toEdit->getNext();
				}

				while (toEdit != nullptr) {
					if (toEdit->getLower()  != nullptr) {
						toEdit->setLower(toEdit->getLower()->getPrevious());

						// This can be useful for debugging higher levels, dosent impact time complexity
						toEdit->setData(toEdit->getLower()->getData());
					}
					// this will be null if we have not set the node yet (ie the last node).
					toEdit = toEdit->getNext();
				}

				if (upperNode != nullptr) {
					upperNode->setLower(this->tail);

					// This can be useful for debugging higher levels
					upperNode->setData(upperNode->getLower()->getData());
				}

			}
			// Everything should be properly placed now.
		}
		return addedNode;
	}


	/**
	 * Gets a reference to a node at an index.
	 * This is the elvator-linked-list implementation, using the upper structure
	 * to speed access when possible
	 */
	virtual Node<T>* getReference(int index) override {

		if (index < 0 || index > this->_size - 1) {
			ostringstream oss;
			oss << "Index was out of bounds: " << index << endl;
			throw std::invalid_argument(oss.str());
		}

		Node<T>* ref;
		int currentIndex;

		if (index <= this->_size / 2) {
			// approach from left
			currentIndex = 0;
			ref = this->head;

		} else {
			// approach from right
			currentIndex = this->size() - 1;
			ref = this->tail;

		}

		// Recurse upwards to jump to the closest index
		if ((std::abs(currentIndex - index) + 1) / this->threshold > 0) {
			// Go into the upper list's linkedlist

			// We need -1 (and +1) as 0 means no upper, 1 means 0th element of upper, so on
			// The first (0) element of the upper list is the [threshold] element of us
			ref = upper->getReference(getUpperIndex(index))->getLower();
			currentIndex = (((index + 1)) / this->threshold) * this->threshold - 1;
		}

		// Iterate backwards if needed
		while (currentIndex > index) {
			// cout << "Iteration!" << endl;
			ref = ref->getPrevious();
			currentIndex--;
		}
		// Iterate forwards if needed
		while (currentIndex < index) {
			// cout << "Iteration!" << endl;
			ref = ref->getNext();
			currentIndex++;
		}
		return ref;
	}


private:
	// Init the upper elevator_linked_list if needed.
	virtual void setupUpper() {
		if(upper == nullptr) {
			upper = new ElevatorLinkedList<T>(this->threshold);
		}
	}

	// Gets the corresponding index from the upper linkedlist
	// Prefers lower values when there is a tie.
	int getUpperIndex(int index) {
		return std::max(0, (index + 1) / this->threshold - 1);
	}

};
