
/*
  elevator-linked-list is a protoype linked list based on how elevators in
  tall buildings are built.

  Copyright (C) 2015  Jay Kamat

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * A class to represent a normal doubly linked linkedlist
 *
 * @author Jay Kamat
 * @version 1.0
 */

#pragma once

#include <stdexcept>
#include <sstream>
#include <iostream>
#include "Node.hpp"
#include <vector>
using namespace std;

template <class T>
class LinkedList {

public:
	LinkedList() {
		this->head = nullptr;
		this->tail = nullptr;
		this->_size = 0;
	}

	virtual ~LinkedList() {
		if (this->size() > 0) {
			Node<T>* ref = head;

			while (ref != nullptr) {
				Node<T>* toDel = ref;
				ref = ref->getNext();
				delete toDel;
			}
		}
	}

	/**
	 * Appends to the list
	 */
	virtual void add(int index, T data) {
		this->internalAdd(index, data);
	}

	/**
	 * Appends to the list
	 */
	virtual void add(T data) {
		this->add(size(), data);
	}

	virtual T remove(int index) {
		if (index < 0 || index > this->_size - 1) {
			ostringstream oss;
			oss << "Index was out of bounds: " << index << endl;
			throw std::invalid_argument(oss.str());
		}

		T toReturn;

		if (this->_size == 1) {
			toReturn = this->head->getData();
			delete this->head;
			this->head = nullptr;
			this->tail = nullptr;
		} else {
			Node<T>* ref = this->getReference(index);
			toReturn = ref->getData();
			if (ref->getPrevious() != nullptr) {
				ref->getPrevious()->setNext(ref->getNext());
			} else {
				head = ref->getNext();
			}

			if (ref->getNext() != nullptr) {
				ref->getNext()->setPrevious(ref->getPrevious());
			} else {
				tail = ref->getPrevious();
			}
			delete ref;
		}

		this->_size--;
		return toReturn;
	}

	virtual T get(int index) {
		if (index < 0 || index > size() - 1) {
			ostringstream oss;
			oss << "Index was out of bounds: " << index << endl;
			throw std::invalid_argument(oss.str());
		}

		Node<T>* ref = getReference(index);
		return ref->getData();
	}

	virtual string toString() {
		ostringstream oss;
		oss << "[";
		Node<T>* ref = head;

		while (ref != nullptr) {
			// cout << ref->getData() << endl;
			oss << ref->getData();
			if (ref != tail) {
				oss << ", ";
			}
			ref = ref->getNext();
		}
		oss << "]";
		return oss.str();
	}

	virtual vector<T>& toVector() {
		vector<T>& toReturn = *(new vector<T>(this->_size));

		Node<T>* ref = head;
		int counter = 0;

		while (ref != nullptr) {
			toReturn[counter++] = ref->getData();
			ref = ref->getNext();
		}

		return toReturn;
	}

	virtual int size() {
		return this->_size;
	}

	virtual T operator[] (unsigned int i) {
		return get(i);
	}

protected:
	Node<T>* head;
	Node<T>* tail;
	int _size;

	/**
	 * Gets a reference to a node at an index. This will be overridden.
	 */
	virtual Node<T>* getReference(int index) {
		if (index < 0 || index > this->_size - 1) {
			ostringstream oss;
			oss << "Index was out of bounds: " << index << endl;
			throw std::invalid_argument(oss.str());
		}

		Node<T>* ref;
		if (index <= this->_size / 2) {
			// approach from left
			int currentIndex = 0;
			ref = this->head;
			while (currentIndex < index) {
				ref = ref->getNext();
				currentIndex++;
			}
		} else {
			// approach from right
			int currentIndex = this->_size - 1;
			ref = this->tail;
			while (currentIndex > index) {
				ref = ref->getPrevious();
				currentIndex--;
			}
		}
		return ref;
	}

	virtual Node<T>* internalAdd(int index, T& data) {
		if (index < 0 || index > this->_size) {
			ostringstream oss;
			oss << "Index was out of bounds: " << index << endl;
			throw std::invalid_argument(oss.str());
		}

		Node<T>* toAdd = new Node<T>(data);

		if (this->_size == 0) {
			head = toAdd;
			tail = toAdd;
		} else if (index == 0) {
			toAdd->setNext(head);
			head->setPrevious(toAdd);
			head = toAdd;
		} else {
			Node<T>* ref = getReference(index - 1);
			toAdd->setPrevious(ref);
			toAdd->setNext(ref->getNext());

			if (ref->getNext() != nullptr) {
				ref->getNext()->setPrevious(toAdd);
			} else {
				tail = toAdd;
			}
			ref->setNext(toAdd);
		}
		this->_size++;
		return toAdd;
	}
};
