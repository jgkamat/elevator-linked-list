/**
 * A main class to test and demo the linked list.
 *
 * @author Jay Kamat
 */

#include <iostream>
#include "LinkedList.hpp"
#include "ElevatorLinkedList.hpp"

using namespace std;

int main() {
	cout << "Starting Normal LinkedList Demo" << endl;
	// Use a normal linkedlist
	LinkedList<int>* list_ptr = new LinkedList<int>();
	LinkedList<int>& list = *list_ptr;

	list.add(1);
	list.add(2);
	list.add(3);
	list.add(4);
	list.add(5);
	list.add(6);
	list.add(7);
	list.add(8);
	list.add(9);
	list.add(10);

	// list.remove(1);

	cout << list.toString() << endl;
	cout << list[4] << endl;
	cout << list[5] << endl;
	// assumes the second block is public
	// cout << n->getNext()->getData() << endl;

	delete list_ptr;

	cout << "Starting Elevator LinkedList Demo" << endl;
	// Use an elevator linkedlist
	ElevatorLinkedList<int>* elevator_ptr = new ElevatorLinkedList<int>(2);
	ElevatorLinkedList<int>& elevator = *elevator_ptr;

	for (int i = 0; i < 10; i++) {
		elevator.add(i);
	}

	// for (int i = 0; i < elevator.size(); i ++) {
	// 	cout << elevator[i] << endl;
	// }

	// NOTE, LEVELS ABOVE THE FIRST SKYLOBBY ARE NOT UPDATED.
	// THIS IS INTENTIONAL, AS IT WOULD SLOW THE PROGRAM DOWN

	cout << elevator.toString() << endl;
	elevator.add(2, 100);
	cout << elevator.toString() << endl;
	elevator.add(5, 300);
	cout << elevator.toString() << endl;
	elevator.add(12, 500);
	cout << elevator.toString() << endl;

	cout << elevator.toString() << endl;
	cout << elevator.remove(2) << endl;
	cout << elevator.toString() << endl;
	cout << elevator.remove(2) << endl;
	cout << elevator.toString() << endl;
	cout << elevator.remove(2) << endl;
	cout << elevator.toString() << endl;

	// assumes the second block is public
	// cout << n->getNext()->getData() << endl;

	delete elevator_ptr;
}
