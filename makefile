
MAKE_FLAGS=--no-print-directory

all: submodule
	mkdir -p build
	cd build; cmake .. -Wno-dev && make $(MAKE_FLAGS)

all-debug: submodule
	mkdir -p build
	cd build; cmake -D CMAKE_CXX_FLAGS="-g" .. -Wno-dev && make $(MAKE_FLAGS)

clean:
	-cd build && make $(MAKE_FLAGS) clean || true
	rm -rf build

test: all
	./run/_test

test-debug: all-debug
	gdb	./run/_test

run: all
	./run/LinkedList

debug: all-debug
	gdb ./run/LinkedList

submodule:
	git submodule update --init
