# Use phusion/baseimage if problems arise
FROM gcc:4.9
MAINTAINER Jay Kamat

# This dockerimage will serve as a 'static' base for the robocup dockerimage to reduce build time

# Setup apt to be happy with no console input
ENV DEBIAN_FRONTEND noninteractive

# setup apt tools and other goodies we want
RUN apt-get update --fix-missing && apt-get -y install cmake

ADD . /root/elevator-linked-list
WORKDIR /root/elevator-linked-list
RUN make clean
RUN make test

