Elevator Linked List [![Build Status](https://circleci.com/gh/jgkamat/elevator-linked-list.svg?&style=svg)](https://circleci.com/gh/jgkamat/elevator-linked-list)
====================

Elevator Linked List is an attempt at using the design of elevators in tall buildings to increase the performance of a random access on a linked list. Is has much faster access than a normal linkedlist, but pays a price in terms of space complexity, and is O(n) for insertion/removal from the front. It does have amortized O(1) insertion/removal from the end however. I outline what I think are the full complexities below.

Overall, an Elevator Linked List can be thought of as either a [Skip List](https://en.wikipedia.org/wiki/Skip_list) with a **true** O(log n) random access, or a [Binary Search Tree](https://en.wikipedia.org/wiki/Binary_search_tree) with amortized O(1) append. Keep in mind, even though this is __like__ a Skip List or a BST, this is still a Linked List at heart, and every node has an index, which is used to retrieve it. This is a List implementation, not a Set implementation.

### Background

A 'Common Sense' approach to elevators work in most scenarios. For example, if you have 4 elevators, you assign each one of them their own shaft, and they operate independently. However, this quickly becomes unbearable as the building gets taller. If this approach is used in skyscrapers, most (90%) of the ground floor level will need to dedicated to elevators in order to provide service to all floors.

A novel solution to this [is the skylobby](https://en.wikipedia.org/wiki/Sky_lobby). A skylobby effectively is a restart of the elevator system in a tall building. When someone wants to go to a high floor, they first take an express elevator to the skylobby servicing that floor. From there, they take the local elevator system as they normally would to go to their floor. The biggest advantage is that the express elevators provide high speed access to skylobbies and the ground floor (eliminating waiting for others to get off/on an elevator). The second advantage is that space can be reused. The local elevator shafts servicing the ground floor to the first skylobby can hold another elevator from skylobby 1 to 2. This is amazing from a space saving view, as we can allocate a fixed space for elevators (as the shafts get reused) throughout the entire building, and have the building be as tall as you want. It **scales**.

### Applying to A Linked List

A skylobby technique can be applied to a linked list as well. We can treat each 'floor' as a node in our linked list, and we can build skylobbies to parts of the linked list. In practice, this is just another 'higher' level linked list than the 'ground' one. If the skylobby linkedlist gets long enough, it may need a skylobby as well.

To make this a bit more rigid, we can define an int `threshold`, which will be the amount of nodes we will tolerate to be in our linked list before we will construct another skylobby node. Of course, this is recursive to the skylobby linked list as well.


### Walkthrough of an ADD

For simplicity, lets assume we have a THRESHOLD of 3 and we have a non-recursive skylobby list.

Lets start with an elevator-linked list, and append 2 elements to it (starting from an empty list).

```
(null)
o
o -> o
```

Now we will append one more element to the end of the list:
```
|-------->o
|         |
o -> o -> o
```
A new skylobby was created, to provide access to the third element. When trying to access the third element from now on, we will use the skylobby (knowing each step in the skylobby gives us 3 steps on the data linkedlist).

We could add two more nodes, before another skylobby would be created.

A more complicated and recursive skylobby structure (Threshold = 2 for this example) would look like this:
```
3 +---------------------->o
  |                       |
2 +---------->o<--------->o
  |           |           |
1 +---->o<--->o<--->o<--->o<--->o
  |     |     |     |     |     |
0 o<>o<>o<>o<>o<>o<>o<>o<>o<>o<>o
```

### Running

You can compile by simply running `make`.

You can run tests with `make test` or main.cpp with `make run`.

`make debug` compiles with gdb if you run into issues :smile:.


The main implementation for ElevatorLinkedList is in the file 'ElevatorLinkedList.hpp', supported by a LinkedList.hpp, and a Node.hpp.

### Visualization

The toString method for ElevatorLinkedList will print out every level of the list, making it useful for debugging

The higher levels for this may not contain the correct values, however (these higher nodes only serve a purpose by pointing at the nodes below them, and their data is not monitored).


### Time Complexities

Disclamer: These are all tentative, mainly my initial guesses.

Let t be the 'Threshold' Value.

| Operation | Type   | Time Complexity |
|-----------|--------|-----------------|
| Get       | Random | O(log_t^n)        |
| Insert    | Random | O(n)            |
| Remove    | Random | O(n)            |
| Append    | End    | O(1) (amortized)|
| Remove    | End    | O(1) (amortized)|

Space Complexity: O(n log_t^n)

Worst case, a random GET operation turns into a BST search, as you start at the highest skylobby, and you perform at most one movement operation per skylobby down. Since each skylobby has n/THRESHOLD elements (where n is the list below it), this becomes log access time. A Threshold of 2 is identical to a BST for access performance.

Random Insert/Removal is slow as we must update the skylobby structure. However, if we perform this in a way such that we remove/add only to the end of the upper list, we only need to modify the list directly above the ground one. This makes the running time at worst case O(n/t + log_t^n), or just O(n).

Appending/Removal from the end is easy, as we can just use the tail pointer for the list to provide O(1) access to the current last element. We may need to update the upper list though, but this is O(1) as well, as we are appending/removing from the end of that one as well. Worst case, however, is still O(log n), as we may have to add one element for every skylobby (although this is very rare).

### Bugs

Elevator Linked List is still very new and has not been tested thoroughly! Please report issues by submitting an issue on GH Issues, or submit a PR!

### License

Elevator Linked List is licensed under the GPLv3.
